d3.json('./matchesWonByEveryTeam.json')
.then(data =>{
  let teamData = getData(data);
  let setOfTeams = Object.keys(teamData).filter(data => data!="")
  console.log(setOfTeams);
  console.log(teamData);
  let winOfTeams = Object.values(data);
  let finalTeam = getFinalWinsOfTeam(setOfTeams,winOfTeams)
  Highcharts.chart('container', {
    chart: {
      type: 'column'
    },
    title: {
      text: 'Stacked column chart'
    },
    xAxis: {
      categories: Object.keys(data)
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Total fruit consumption'
      },
      stackLabels: {
        enabled: true,
        style: {
          fontWeight: 'bold',
          color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
        }
      }
    },
    legend: {
      align: 'right',
      x: -30,
      verticalAlign: 'top',
      y: 25,
      floating: true,
      backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
      borderColor: '#CCC',
      borderWidth: 1,
      shadow: false
    },
    tooltip: {
      headerFormat: '<b>{point.x}</b><br/>',
      pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
    },
    plotOptions: {
      column: {
        stacking: 'normal',
        dataLabels: {
          enabled: true,
          color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
        }
      }
    },
    series: finalTeam
  });
})
.catch(console.log)
function getData(data){
  let teamData = {}
  Object.values(data).forEach(element => {
    for(var x in element){
      if(!Boolean(teamData[x])){
        teamData[x] =[];
      }  
      teamData[x].push(element[x]);
    }
  });
  return teamData;
}
function getFinalWinsOfTeam(data1,data2){
  let finalTeam = {}
  data1.forEach((team)=>{
    data2.forEach( (obj)  =>{
      if(team in obj){
        if(finalTeam[team]===undefined){
          finalTeam[team] =[obj[team]]
        }
        else{
          finalTeam[team].push(obj[team])
        }
      }
      else{
        if(finalTeam[team]==undefined){
          finalTeam[team] =[0];
        }
        else{
          finalTeam[team].push(0);
        }
      }     
    });
  });
  let finalTeamArray = [],count=0;
  for(var x in finalTeam){
    finalTeamArray[count]={};
    finalTeamArray[count]['name'] =x;
    finalTeamArray[count]['data'] = finalTeam[x];
    ++count;
  }
  return finalTeamArray;
}