d3.json('./economicPlayer.json')
.then(data =>{
  let dataObj = Object.entries(data).map(data =>{
     return [data[0],parseFloat(data[1])]
  });
  dataObj = dataObj.sort((a,b)=> a[1]===b[1] ? 0 : a[1] > b[1] ? 1: -1);
  let topEconomicalPlayersData = dataObj.slice(0,10)
  let topEconomicalPlayers = topEconomicalPlayersData.map(a => a[0])
  let economicalScore = topEconomicalPlayersData.map(a => a[1]);
  Highcharts.chart('container', {
    chart: {
      type: 'column'
    },
    title: {
      text: 'Extra Runs Conceded Per Team'
    },
    xAxis: {
      categories: topEconomicalPlayers
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Extra Runs Scored'
      }
    },
    legend: {
      reversed: true
    },
    plotOptions: {
      series: {
        stacking: 'normal'
      }
    },
    series: [{
      name: 'IPL Teams',
      data: economicalScore
    }
  ]
  });
})
.catch(console.log)
