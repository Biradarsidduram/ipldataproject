let fs = require('fs')
let matchesJson = require('./matches.json')
let deliveriesJson = require('./deliveries.json')
let numberOfMatchesPlayedOverAll = {};
let matchesWonByEveryTeam  = {};
let obj = [];
let extraRunIds =[];
let extraRun= {};
let extraRunPerTeam = {};
let economicalBorderIds = [];
let economicalBowler = {};
let economicPlayer = {};
matchesJson.forEach(function(value){
	if(Boolean(numberOfMatchesPlayedOverAll[value['season']]))
  		++numberOfMatchesPlayedOverAll[value['season']];
	else 
		numberOfMatchesPlayedOverAll[value['season']]=1	
		  
	if(Boolean(matchesWonByEveryTeam[value['season']])){
		if(Boolean(matchesWonByEveryTeam[value['season']][value['winner']]))
			++matchesWonByEveryTeam[value['season']][value['winner']];
		else
			matchesWonByEveryTeam[value['season']][value['winner']]=1
	}
	else{
		matchesWonByEveryTeam[value['season']] = {};
		matchesWonByEveryTeam[value['season']][value['winner']]=1
	}
	if(parseInt(value['season'])===2016)
		extraRunIds.push(value['id']);
	if(parseInt(value['season'])===2015)
		economicalBorderIds.push(value['id']);
});

fs.writeFileSync('./number of matches played/numberOfMatchesPlayedOverAll.json',JSON.stringify(numberOfMatchesPlayedOverAll),"utf-8");
fs.writeFileSync('./matches won all the team/matchesWonByEveryTeam.json',JSON.stringify(matchesWonByEveryTeam),"utf-8");
extraRunIds.forEach(function(element, index){
	deliveriesJson.forEach( function(value) {
		if(value['match_id']===element){
			if(Boolean(extraRun[value['bowling_team']]))
				extraRun[value['bowling_team']]+=parseInt(value['extra_runs'])
			else
				extraRun[value['bowling_team']]=parseInt(value['extra_runs']);
		}
	});
});
fs.writeFileSync('./extra runs per team/extraRun.json',JSON.stringify(extraRun),'utf-8');
economicalBorderIds.forEach(function(ecoValue){
	deliveriesJson.forEach(function(delValue){
		if(delValue['match_id']===ecoValue){
			if(Boolean(economicalBowler[delValue['bowling_team']])){
				// if a bowler of same team is present..
				if(Boolean(economicalBowler[delValue['bowling_team']][delValue['bowler']])){
					economicalBowler[delValue['bowling_team']][delValue['bowler']]['runs']+=parseInt(delValue['total_runs']);
					if(parseInt(delValue['ball'])===6){
						++economicalBowler[delValue['bowling_team']][delValue['bowler']]['overs'];
					}
				}
				else {
					economicalBowler[delValue['bowling_team']][delValue['bowler']] ={}
					economicalBowler[delValue['bowling_team']][delValue['bowler']]['runs']=parseInt(delValue['total_runs']);

					economicalBowler[delValue['bowling_team']][delValue['bowler']]['overs']=0;
				}
			}
			else
				economicalBowler[delValue['bowling_team']]={};
		}
	});
	for(var x in economicalBowler){
		for(var y in economicalBowler[x]){
			if(!Boolean(economicPlayer[y]))
				economicPlayer[y] = [];
				// overs should not be zero..
				if(economicalBowler[x][y]['overs']!==0)
					economicPlayer[y].push(parseFloat((economicalBowler[x][y]['runs']/economicalBowler[x][y]['overs']).toFixed(3)));
		}
	}
	economicalBowler ={};
});
let len = 0;
for(var x in economicPlayer){
	len = economicPlayer[x].length;
	economicPlayer[x] = (economicPlayer[x].reduce((a,b)=> a+b)/len).toFixed(2);
}
fs.writeFileSync('./economical Bowler/economicPlayer.json',JSON.stringify(economicPlayer),'utf-8');