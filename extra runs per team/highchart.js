d3.json('./extraRun.json')
.then(data =>{
  console.log(Object.keys(data));
  let dates = Object.keys(data).map(data => parseInt(data));
  Highcharts.chart('container', {
    chart: {
      type: 'column'
    },
    title: {
      text: 'Extra Runs Conceded Per Team'
    },
    xAxis: {
      categories: Object.keys(data)
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Extra Runs Scored'
      }
    },
    legend: {
      reversed: true
    },
    plotOptions: {
      series: {
        stacking: 'normal'
      }
    },
    series: [{
      name: 'IPL Teams',
      data: Object.values(data)
    }
  ]
  });
})
.catch(console.log)
