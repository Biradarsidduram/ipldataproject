const csvParser = require('csv-parser');
const fs = require('fs');
let matchesJson = [];
let deliveriesJson = [];
fs.createReadStream('matches.csv').pipe(csvParser())
	.on('data' ,data=> {matchesJson.push(data);}).on('end', () => {
	fs.writeFileSync("matches.json",JSON.stringify(matchesJson))
});

fs.createReadStream('deliveries.csv').pipe(csvParser()).on('data',data =>{deliveriesJson.push(data)}).on('end', () => {
	fs.writeFileSync('deliveries.json',JSON.stringify(deliveriesJson));
});