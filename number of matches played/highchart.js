d3.json('./numberOfMatchesPlayedOverAll.json')
.then(data =>{
  console.log(Object.keys(data));
  let dates = Object.keys(data).map(data => parseInt(data));
  Highcharts.chart('container', {
    chart: {
      type: 'column'
    },
    title: {
      text: 'No.of Matches Played during IPL years'
    },
    xAxis: {
      categories: Object.keys(data)
    },
    yAxis: {
      min: 0,
      title: {
        text: 'No.of Matches Played by Every Team'
      }
    },
    legend: {
      reversed: true
    },
    plotOptions: {
      series: {
        stacking: 'normal'
      }
    },
    series: [{
      name: 'IPL years',
      data: Object.values(data)
    }
  ]
  });
})
.catch(console.log)
